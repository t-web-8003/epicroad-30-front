# EpicRoad-30-Front README

## INSTALLATION

A little intro about the installation.

Commands :
```
git clone git@gitlab.com:t-web-8003/epicroad-30-front.git
cd ../path/to/the/file
npm install
```

**Create a `.env` file** with the same credentials as `.env-example` example. You can find API keys in the README.md of the EpicRoad main project.

```
npm run serve
```

## TEST

To run the test run the following command

Command :
```
npm run test:unit
```
You will find the jest configuration in the file jest.config.js <br> The folder containing the test files is tests\unit

## TECHNOLOGIES

* Language: Vue.js
* Test: Jest
